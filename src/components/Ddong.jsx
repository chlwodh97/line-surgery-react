import React, {useState} from "react";

const Ddong = () => {
    const [currentQuestion,setCurrentQuestion] = useState(0)
    const [score,setScore] = useState(0)

    const questions = [
        {
            img : "./images/ddong1.jpg",
            question: "최근 2일동안 대변을 보셨나요?",
            answer: [
                { text : "예" , score : 0},
                { text : "아니오", score : 0},
            ],
        },
        {
            img : "이미지2",
            question: "시큼하거나 비린냄새가 나나요?",
            answer: [
                { text : "예" , score : 10},
                { text : "아니오", score : 0},
            ],
        },
        {
            img : "이미지3",
            question: "변이 길게 나오나요 짧게 나오나요?", // 물판단불가
            answer: [
                { text : "길게" , score : 0},
                { text : "짧게", score : 10},
            ],
        },
        {
            img : "이미지4",
            question: "최근 일주일동안 피가 묻어나온적이 몇 번 인가요 ?",
            answer: [
                { text : "0 번" , score : 0},
                { text : "1~2 번", score : 5},
                { text : "3~4 번", score : 10},
                { text : "5번 이상", score : 70},
            ],
        },
        {
            img : "이미지5",
            question: "내 변은 어떤 가요?",
            answer: [
                { text : "딱딱하다" , score : 10},
                { text : "정상이다", score : 0},
                { text : "묽다", score : 10},
            ],
        },
        {
            img : "이미지6",
            question: "내 변의 색은 무슨색?",
            answer: [
                { text : "노랑", score : 10},
                { text : "초록", score : 10},
                { text : "검정", score : 70},
                { text : "빨강", score : 70},
                { text : "회백", score : 30},
                { text : "황갈", score : 0},
            ],
        },
        {
            img : "이미지7",
            question: "최근 한달 동안 항문외과 방문 이력이 있나요?",
            answer: [
                { text : "예" , score : 10},
                { text : "아니오", score : 0},
            ],
        },
    ]

    // handleAnswerClick else 문으로 마지막 질문이 끝나면 겟 리절트 호출 된다
    const getResult = score => {
        // 스코어가 45점 보다 높으면
        if (score >= 45) {
            return "지금 당장 항문외과를 방문하세요"
        } else {
            return "당신의 장은 건강하네요"
        }
    }

    // 버튼 클릭시 호출 되는 함수
    const handleAnswerClick = answerScore => {
        // 클릭시 스코어 useState score 값을 score + 매개변수 점수를 더한다
        setScore(score + answerScore)

        // 다음 질문은 현재 질문 + 1
        const nextQuestion = currentQuestion + 1

        // 총 질문의 길이가 다음 질문의 길이보다 크면 -> 아직 질문이 남아있으면
        if (nextQuestion < questions.length) {

            // 유즈 스테이트 현재 질문에 (다음 질문 담는다)
            setCurrentQuestion(nextQuestion)
        } else {
            // -> 다음 질문이 없으면 -> 마지막질문이면
            // 그렇지 않은 경우엔 getResult 함수에 점수 + 답변점수를 넘겨준다
            alert(getResult(score + answerScore))
        }
    }

    return(
        <div>
            {/* 질문목록에서 현재 질문에서 질문을 꺼내온 것*/}
            <h2>{questions[currentQuestion].question}</h2>
            {/*질문목록에서 현재 질문에서 답변을 map을 함(배열이니까 map가능)*/}
            {questions[currentQuestion].answer.map((answer) => (
                // 위 배열에서 map 으로 다시 배열을 만드는데
                // 키는 답변에 텍스트이면서 버튼 내용에도 담긴다
                // 그리고 클릭하면서 저 함수 호출 하는데 점수줌
                <button key={answer.text} onClick={()=> handleAnswerClick(answer.score)}>
                    {answer.text}
                </button>
            ))}
        </div>
    );
}

export default Ddong;